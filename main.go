package main

import (
	b64 "encoding/base64"
	"encoding/hex"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"crypto/rand"
	"crypto/subtle"

	"golang.org/x/crypto/chacha20poly1305"
	"golang.org/x/crypto/ed25519"
	"github.com/o1egl/paseto"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"

	_ "github.com/joho/godotenv/autoload"
	lr "github.com/sirupsen/logrus"

)

const (
	KeySize    = 32
	NonceSize  = 12
	NonceSizeX = 24
	Overhead   = 16
)

type SigningRequest struct {
	URL      string `json:"url" xml:"url" form:"url"`
	Username string `json:"username" xml:"username" form:"username"`
	Password string `json:"password" xml:"password" form:"password"`
	Sub      string `json:"sub" xml:"sub" form:"sub"`
	Pub      string `json:"pub" xml:"pub" form:"pub"`
	Userid   string `json:"userid" xml:"userid" form:"userid"`
	Expday   int    `json:"expday" xml:"expday" form:"expday"`
}

type SigingConfig struct {
	Audience   string
	Issuer     string
	Jti        string
	Subject    string
	ExpDay     int
	PublicKey  ed25519.PublicKey
	PrivateKey ed25519.PrivateKey
	Footer     string
}

var signingConfig SigingConfig

type PubkeyFormats struct {
	Openssl string `json:"openssl" xml:"openssl" form:"openssl"`
	Hex     string `json:"hex" xml:"hex" form:"hex"`
	Base64  string `json:"base64" xml:"base64" form:"base64"`
}

type Verify struct {
	Token string `json:"token" xml:"token" form:"token"`
}

type SignUserSession struct {
	Userid    string `json:"userid" xml:"userid" form:"userid"`
	Privilege string `json:"privilege" xml:"privilege" form:"privilege"`
	Expday    int    `json:"expday" xml:"expday" form:"expday"`
}

var pubkeyFormats PubkeyFormats

func init() {
	lr.SetFormatter(&lr.TextFormatter{FullTimestamp: true})
	lr.SetOutput(os.Stdout)

	if len(os.Getenv("PUBLIC_KEY")) == 0 || len(os.Getenv("PRIVATE_KEY")) == 0 {
		publicKey, privateKey, _ := ed25519.GenerateKey(nil)
		lr.Info("---------------------------------------------------------------------------------------------------------")
		lr.Info("| Generate golang compatible ed25519 keypair")
		lr.Info("| Public key: ", b64.StdEncoding.EncodeToString(publicKey))
		lr.Info("| Private key: ", b64.StdEncoding.EncodeToString(privateKey))
		lr.Info("---------------------------------------------------------------------------------------------------------")
		os.Exit(1)
	}

	pk, err := b64.StdEncoding.DecodeString(os.Getenv("PUBLIC_KEY"))
	if err != nil {
		lr.Fatal(err)
	}
	publicKeyHex := hex.EncodeToString([]byte(os.Getenv("PUBLIC_KEY")))
	signingConfig.PublicKey = ed25519.PublicKey(pk)
	sk, err := b64.StdEncoding.DecodeString(os.Getenv("PRIVATE_KEY"))
	if err != nil {
		lr.Fatal(err)
	}
	signingConfig.PrivateKey = ed25519.PrivateKey(sk)
	signingConfig.Audience = os.Getenv("SIGN_AUDIENCE")
	signingConfig.Issuer = os.Getenv("SIGN_ISSUER")
	signingConfig.Jti = os.Getenv("SIGN_JTI")
	signingConfig.Subject = os.Getenv("SIGN_SUBJECT")
	signingConfig.ExpDay, err = strconv.Atoi(os.Getenv("SIGN_EXPDAY"))
	if err != nil {
		lr.Fatal(err)
	}
	signingConfig.Footer = os.Getenv("SIGN_FOOTER")

	pubkeyFormats.Openssl = fmt.Sprintf("-----BEGIN PUBLIC KEY-----\n%s\n-----END PUBLIC KEY-----", os.Getenv("PUBLIC_KEY"))
	pubkeyFormats.Base64 = os.Getenv("PUBLIC_KEY")
	pubkeyFormats.Hex = publicKeyHex
}

func signAndCrypt(c *fiber.Ctx) error {
	sr := new(SigningRequest)

	if err := c.BodyParser(sr); err != nil {
		return err
	}

	var claims = map[string]string{
		"URL":      sr.URL,
		"Username": sr.Username,
		"Password": sr.Password,
		"Pub":      sr.Pub,
		"Sub":      sr.Sub,
		"Userid":   sr.Userid,
	}

	signedToken := Signer(claims, sr.Expday)

	cipherText, key := encrypt(signedToken)

	return c.Status(200).JSON(fiber.Map{"token": cipherText, "key": key})
}

func pub(c *fiber.Ctx) error {
	return c.JSON(pubkeyFormats)
}

func signClient(c *fiber.Ctx) error {
	sr := new(SigningRequest)

	if err := c.BodyParser(sr); err != nil {
		return err
	}

	var claims = map[string]string{
		"URL":      sr.URL,
		"Username": sr.Username,
		"Password": sr.Password,
		"Pub":      sr.Pub,
		"Sub":      sr.Sub,
		"Userid":   sr.Userid,
	}

	signedToken := Signer(claims, sr.Expday)

	return c.Status(200).SendString(signedToken)
}

func sign(c *fiber.Ctx) error {
	sr := new(SignUserSession)

	if err := c.BodyParser(sr); err != nil {
		return err
	}

	var claims = map[string]string{
		"Userid":    sr.Userid,
		"Privilege": sr.Privilege,
	}

	signedToken := Signer(claims, sr.Expday)

	return c.Status(200).SendString(signedToken)
}

func verify(c *fiber.Ctx) error {
	verify := new(Verify)

	if err := c.BodyParser(verify); err != nil {
		return err
	}

	var newJsonToken paseto.JSONToken
	var newFooter string
	V2 := paseto.NewV2()
	err := V2.Verify(verify.Token, signingConfig.PublicKey, &newJsonToken, &newFooter)
	if err != nil {
		return c.Status(401).SendString(err.Error())
	} else {
		return c.Status(200).SendString("OK")
	}
}

//Authorization checking
//Check Authorization and X-Auth-Token header
//Subtle package help to compare the two string
func auth(c *fiber.Ctx) error {
	if len(c.Get("Authorization")) > 0 || len(c.Get("X-Auth-Token")) > 0 {
		if len(c.Get("Authorization")) > 0 {
			token := strings.Split(c.Get("Authorization"), "Bearer ")
			if subtle.ConstantTimeCompare([]byte(token[1]), []byte(os.Getenv("BEARER_TOKEN"))) == 1 {
				return c.Next()
			} else {
				return c.SendStatus(401)
			}
		} else {
			if subtle.ConstantTimeCompare([]byte(c.Get("X-Auth-Token")), []byte(os.Getenv("BEARER_TOKEN"))) == 1 {
				return c.Next()
			} else {
				return c.SendStatus(401)
			}
		}
	} else {
		return c.SendStatus(401)
	}
}

func main() {
	lr.Info("Init fiber...")
	app := fiber.New()

	app.Use(recover.New())

	api := app.Group("/api")
	v1 := api.Group("/v1")
	v2 := api.Group("/v2")

	v1.Get("/pub", pub)
	v1.Post("/verify", verify)

	app.Use(auth)
	v1.Post("/sign/session", sign)
	v1.Post("/sign/client", signClient)
	v2.Post("/sign/client", signAndCrypt)

	lr.Info("Listener started...")
	lr.Fatal(app.Listen(":3000"))
}

func encrypt(plainToken string) (cipherText, secretKey string) {
	key := make([]byte, KeySize)
	if _, err := rand.Read(key); err != nil {
		panic(err)
	}

	aead, err := chacha20poly1305.NewX(key)
	if err != nil {
		panic(err)
	}

	// Encryption.
	var encryptedMsg []byte
	msg := []byte(plainToken)

	// Select a random nonce, and leave capacity for the ciphertext.
	nonce := make([]byte, aead.NonceSize(), aead.NonceSize()+len(msg)+aead.Overhead())
	if _, err := rand.Read(nonce); err != nil {
		panic(err)
	}
	// Encrypt the message and append the ciphertext to the nonce.
	encryptedMsg = aead.Seal(nonce, nonce, msg, nil)
	cipherText = hex.EncodeToString(encryptedMsg)
	secretKey = hex.EncodeToString(key)

	return cipherText, secretKey
}

func Signer(claims map[string]string, expDay int) (token string) {

	now := time.Now()
	exp := time.Now().Add(time.Duration(expDay) * 24 * time.Hour)
	nbt := now
	jsonToken := paseto.JSONToken{
		Audience:   signingConfig.Audience,
		Issuer:     signingConfig.Issuer,
		Jti:        signingConfig.Jti,
		Subject:    signingConfig.Subject,
		IssuedAt:   now,
		Expiration: exp,
		NotBefore:  nbt,
	}

	V2 := paseto.NewV2()

	for key, element := range claims {
		jsonToken.Set(key, element)
	}

	// Sign data
	token, err := V2.Sign(signingConfig.PrivateKey, jsonToken, signingConfig.Footer)
	if err != nil {
		lr.Fatal(err)
	}
	return token
}
