FROM golang:1.17.3-alpine3.14 as build
RUN apk add --no-cache gcc libc-dev ca-certificates && update-ca-certificates
WORKDIR /app

ENV CGO_ENABLED=0
ENV GO111MODULE=on
#ENV GOFLAGS=-mod=vendor

COPY go.mod go.sum ./
RUN go mod download
COPY . .

RUN go build -o /app/signer .

FROM scratch AS final
LABEL maintainer="D3v <mark@zsibok.hu>"
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /app/signer /

EXPOSE 3000
CMD [ "./signer" ]